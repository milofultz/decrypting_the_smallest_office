# Canvas Bookmarklet

This is an attempt to understand the `canvas` code within zserge's [World's Smallest Office Suite](https://zserge.com/posts/awfice/]). I've been using this thing for over a year for general sketching out of ideas when I don't have paper, but didn't have any idea how it really worked, but now I do!

